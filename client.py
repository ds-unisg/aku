import base64
import json
import httpx

MODEL = "V2-AKU-220x220-C3232P2D-C64C64P2D-F-D256D256D_label50"
IMAGE_PATH = "/home/bernhard/Arbeit/Data/AKU/data/version_2/jpg/B060_6616.jpg"

# defining the api-endpoint
API_ENDPOINT = "http://localhost:5000/predict"

# Encoding the JPG,PNG,etc. image to base64 format
with open(IMAGE_PATH, "rb") as image_file:
    b64_image = base64.b64encode(image_file.read())

# data to be sent to api
data = {
    'b64': b64_image.decode(),
    'model': MODEL
}

# sending post request and saving response as response object
r = httpx.post(url=API_ENDPOINT, json=data)

try:
    print(json.dumps(r.json()))
except:           
    print(r.content)
