

# AKU Classification
_Bernhard Bermeitinger, bernhard.bermeitinger@unisg.ch_

## Requirements
All code is written and designed for **Python 3.7**. It is not tested with any other version or on any other OS than Linux x86_64.
A CUDA-enabled card is not required but recommended, although the "simple" networks can run on normal CPUs as well.
The TensorFlow version is **2.0.0b1**.  (Specifically, it is _not_ compatible with TF1)
If using a CUDA-card, TensorFlow requires CUDA 10.0. The newer version 10.**1**. does not work and TensorFlow must be compiled against it manually.
It is intalled with
`pip install tensorflow==2.0.0b1`
or for GPUs
`pip install tensorflow-gpu==2.0.0b1`

Additional Python modules should be installed with
`pip install -U -r requirements.txt`

When using Jupyter Lab (instead of Notebook) and having nice progressbars (with tqdm), NodeJS must be installed and the following code must be run:
```bash
jupyter serverextension enable --py jupyter_http_over_ws
jupyter nbextension enable --py widgetsnbextension
jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter lab build
```
## Notebooks
The Jupyter Notebooks are in `notebooks/`.
You have to adjust the folders (input, output, images) in their respective variables right after the import statements for each notebook.
Also, sometimes there is parallel execution of functions, the number of parallel jobs is hard-coded to a very high value (e.g. 32). On a small machine, like a PC, this will be overwhelming, so please adjust these settings prior to running the whole notebook.

## Experiment
The code here is using a "simple" convolutional neural network. It has still a lot of parameters but compared to VGG16, VGG19, or a ResNet, the number of parameters is very low. The reason for this is the relatively small input space. For a bigger network, the input space would fit fully into the parameter space which makes generalization impossible.

The training is done in `notebooks/AKU-v2-Training.ipynb`.

### Architecture
The architecture is:

 - Input Layer
 - Convolutional Layer (32 Filters, 3x3 Kernel)
 - Convolutional Layer (32 Filters, 3x3 Kernel)
 - Max-Pooling Layer ( 2x2 Pooling size)
 - Dropout (0.25 Rate)
 - Convolutional Layer (64 Filters, 3x3 Kernel)
 - Convolutional Layer (64 Filters, 3x3 Kernel)
 - Max-Pooling Layer ( 2x2 Pooling size)
 - Dropout (0.25 Rate)
 - Flatten
 - Dense (256 Hidden units)
 - Dense (256 Hidden units)
 - Dropout (0.50 Rate)
 - Classification (Dense with #number_of_classes hidden units and softmax activation)

All Convolutional and Dense layers are using ReLU as their activation function, except, of course, the last Dense layer which uses the Softmax.

### Input
#### Data
The classes are chosen either _label_ or _category_:
Label is the actual label of the script (e.g. _A010_), Category is the script's category (e.g. _A_)
There are 6 different configurations preconfigured:
 1. `label2`: Using *label* and only those that appear at least _2_ times in the dataset
 2. `label10`: Using *label* and only those that appear at least _10_ times in the dataset
 3. `label20`: Using *label* and only those that appear at least _20_ times in the dataset
 4. `label50`: Using *label* and only those that appear at least _50_ times in the dataset
 5. `category2`: Using *category* and only those that appear at least _2_ times in the dataset
 6. `category5`: Using *category* and only those that appear at least _5_ times in the dataset

The notebook `notebooks/AKU-v2-DataPreparation-TFRecord.ipynb` creates a `tf.RecordFile` for each configured `ExperimentConfig`. Additionaly, it also stores the random train/test-split as a csv.

The labels/categories (=classes) are converted to numbers using `sklearn`'s `LabelEncoder`. This label encoder is stored as a pickle for future loading (as can be seen in `notebooks/AKU-v2-Predictions.ipynb`.

#### Network
The chosen network takes 220x220 images with 3 channels. Although the images are black and white and don't contain color information, it is required for TensorFlow's image processing for augmentation to be in three dimensions.

The batch size is set to 128. You can adjust is according to your GPU memory limits.

The images are augmented to simulate a bigger input space:

 - random flip left right
 - random hue change by at maximum 0.08
 - random saturation change from 0.6 to 1.6
 - random brightness change by maximum 0.05
 - random contrast change from 0.7 to 1.3
 - random jpeg quality from 75 to 95
 - random image crop of 90% of the image


### Output
The output is a probability distribution over available classes. Usually, the highest one will be taken and converted back to the class for easier analysis.
During training, `TensorBoard` is used for watching the progress. It must be started manually to access.

# Results
The notebook `notebooks/AKU-v2-Predictions.ipynb` will create the plots for accuracy, precision, recall, and f1-score for each configuration
(Precision, Recall, and F1-Measure is using the _weighted_ average.)
