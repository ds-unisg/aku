import base64
import os
from io import BytesIO
from pathlib import Path
from typing import Dict
from typing import List
from typing import Tuple

import httpx
import joblib
import numpy as np
import sklearn
from sklearn.preprocessing import LabelEncoder
from quart import Quart, jsonify, request, make_response
from quart_cors import cors
from PIL import Image
import logging

logging.basicConfig(level=logging.DEBUG)

log = logging.getLogger('app')

app = Quart(__name__)
app = cors(app, allow_origin="*", allow_headers="*", allow_methods="*")

# Uncomment this line if you are making a Cross domain request
#CORS(app)

_DATA_DIR = Path(os.getenv('DATA_DIR', "generated"))

_NUM_CHANNELS: int = 3
_IMAGE_SIZE: Tuple[int, int] = (220, 220)
_MODELS: List[str] = [
    'V2-AKU-220x220-C3232P2D-C64C64P2D-F-D256D256D_category2',
    'V2-AKU-220x220-C3232P2D-C64C64P2D-F-D256D256D_category5',
    'V2-AKU-220x220-C3232P2D-C64C64P2D-F-D256D256D_label2',
    'V2-AKU-220x220-C3232P2D-C64C64P2D-F-D256D256D_label10',
    'V2-AKU-220x220-C3232P2D-C64C64P2D-F-D256D256D_label20',
    'V2-AKU-220x220-C3232P2D-C64C64P2D-F-D256D256D_label50',
]
_LBL_ENC: Dict[str, LabelEncoder] = {
    model: joblib.load(str(_DATA_DIR.joinpath(f"label-encoder_{model.split('_')[1]}.tar.bz2"))) for model in _MODELS
}


def image_loading(encoded_image):
    image = Image.open(encoded_image)
    image = image.convert('RGB')
    image = image.resize(_IMAGE_SIZE)
    image = np.array(image)
    image = np.abs(image - 255.0)
    image /= 255.0
    image = image.astype(np.float16)

    return image.tolist()


# Testing URL
@app.route('/ping', methods=['GET', 'POST'])
async def hello_world():
    return await make_response(jsonify({'status': 'ok'}))


@app.route('/models', methods=['GET'])
async def get_available_models():
    return await make_response(jsonify({'models': _MODELS}))


@app.route('/predict', methods=['POST'])
async def image_classifier():
    data = await request.json

    log.info("Request data: %s", data)
    
    img_str = data['b64']

    if img_str.startswith('data'):
        img_str = img_str[len('data:image/jpeg;base64,'):]

    img_b64 = base64.b64decode(img_str.encode())
    
    log.info(img_b64)
    
    img_bytes = BytesIO(img_b64)

    log.info(img_bytes)
    
    img = image_loading(img_bytes)

    model = data['model']

    # Creating payload for TensorFlow serving request
    payload = {
        "instances": [img]
    }

    # Making POST request
    async with httpx.AsyncClient() as c:
        r = await c.post(f'http://tfs:9000/v1/models/{model}:predict', json=payload)

    # Decoding results from TensorFlow Serving server
    result = r.json()
    raw_predictions = np.array(result['predictions'])[0]

    ordered = list(reversed(np.argsort(raw_predictions)))

    predictions = _LBL_ENC[model].inverse_transform(ordered)
    confidences = [np.round(c, 5) for c in raw_predictions[ordered]]

    # Returning JSON response to the frontend
    return await make_response(jsonify({
        "model": model,
        "prediction": predictions[0],
        "confidence": confidences[0],
        "_all": [
            (p, c) for p, c in zip(predictions, confidences)
        ]
    }))
